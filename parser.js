const mysql   = require('mysql');
const request = require('request');
const cheerio = require('cheerio');

// Définition de la connexion à la BDD :
const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'wintips'
});


function parseGames(callback) {
    /*
     * Méthode de parsing du code HTML de Betclic
     */
    let gamesArray = [];

    // On requête la page de Betclic :
    request('https://www.betclic.fr/calendrier-0', function (error, response, html) {
        if (!error && response.statusCode == 200) {
            // On utilise le package "cheerio" pour parser le HTML :
            const $ = cheerio.load(html);
    
            // Récupère tous les éléments du HTML qui ont la classe "cal-day-entry" :
            const days = $('.cal-day-entry');
            // On parcourt chaque élément un par un (ça correspond à chaque jour) :
            days.each(function(index, day) {
                // On récupère la date du match qui se trouve dans une balise <span> :
                const date = $(day).find('.cal-day > span').text();
                // On récupère tous les matchs de la journée :
                const games = $(day).find('.cal-schedule');
                // On parcourt chaque match :
                games.each(function(index, match) {
                    // On récupère l'heure du match qui se trouve dans un élément qui a la classe "cal-hour" :
                    const hour = $(match).find('.cal-hour').text();
                    // On récupère le match qui se trouve dans l'attribut de l'élément qui a pour classe "match-entry" :
                    const matchName = $(match).find('.event .match-entry').attr('data-track-event-name');
                    // On split le nom du match pour séparer les teams :
                    const teams = matchName.split(' - ');
                    
                    // On met tout ce qu'on a récupéré dans un tableau "gamesArray" :
                    gamesArray.push({
                        date: date,
                        hour: hour,
                        homeTeam: teams[0],
                        visitorTeam: teams[1],
                    });
                });
            });

            // On appelle la méthode qui va enregistrer en BDD :
            callback(gamesArray);
        } else {
            console.log(error);
        }
    });
}


parseGames(function(games) {
    // Connexion à la BDD :
    connection.connect();

    // Crée la table "games" si elle n'existe pas déjà :
    connection.query(' \
        CREATE TABLE IF NOT EXISTS games ( \
            id INTEGER NOT NULL AUTO_INCREMENT, \
            date VARCHAR(255) NOT NULL, \
            hour VARCHAR(255) NOT NULL, \
            home_team VARCHAR(255) NOT NULL, \
            visitor_team VARCHAR(255) NOT NULL, \
            PRIMARY KEY id(id) \
        ) ENGINE=INNODB DEFAULT CHARSET = utf8;', 
        function(error, results, fields) {
            // En cas d'erreur, on l'affiche :
            if (error) throw error;
        }
    );

    // On parcourt tous les matchs qu'on a récupéré précédemment dans la méthode "parseGames" :
    games.forEach(function(game) {
        // On insère les résultats dans la BDD :
        connection.query(' \
            INSERT INTO games (date, hour, home_team, visitor_team) \
            VALUES ( \
                "' + game.date + '", \
                "' + game.hour + '", \
                "' + game.homeTeam + '", \
                "' + game.visitorTeam + '" \
            );', 
            function(error, results, fields) {
                // En cas d'erreur, on l'affiche :
                if (error) throw error;
                console.log('A new game has been added to the database.');
            }
        );
    });

    // Fermeture de la connexion à la BDD :
    connection.end();
});
